package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowers = domController.loadData();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		Collections.sort(flowers.getFlowers(),
				domController.compareFlowerByName);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.setXmlFileName(outputXmlFile);
// переробити вивід з кореня
		domController.saveToXml(flowers);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
/*		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
//		SAXController saxHandler = this;//new SAXController("");
		saxParser.parse(xmlFileName, saxController);
*/
		flowers = saxController.loadData();

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Collections.sort(flowers.getFlowers(),
				new DOMController("").compareFlowerBySoil);

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.setXmlFileName(outputXmlFile);
		saxController.writeToXml(flowers);

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		flowers = staxController.loadData();

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Collections.sort(flowers.getFlowers(),
				new DOMController("").compareFlowerByOrigin);

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.setXmlFileName(outputXmlFile);
		staxController.saveToXml(flowers);
	}

}
