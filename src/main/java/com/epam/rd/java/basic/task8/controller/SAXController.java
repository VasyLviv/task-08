package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	private Flowers flowers;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private StringBuilder elementValue;


	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (elementValue == null) {
			elementValue = new StringBuilder();
		} else {
			elementValue.append(ch, start, length);
		}
	}

	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void writeToXml(Flowers flowers) throws UnsupportedEncodingException, XMLStreamException, FileNotFoundException {
		STAXController staxController = new STAXController(xmlFileName);
		staxController.saveToXml(flowers);
/*		OutputStream outputStream = new FileOutputStream(new File(xmlFileName));

		XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
				new OutputStreamWriter(outputStream, "utf-8"));

		out.writeStartDocument();
		out.writeStartElement("flowers");
		for(Map.Entry<String, String> att:flowers.getAttributes().entrySet()) {
			out.writeAttribute(att.getKey(), att.getValue());
		}

		for (Flower fl: flowers.getFlowers()) {
			out.writeStartElement("flower");
				out.writeStartElement("name");
				out.writeCharacters(fl.getName());
				out.writeEndElement();

				out.writeStartElement("soil");
				out.writeCharacters(fl.getSoil().getTitle());
				out.writeEndElement();

				out.writeStartElement("origin");
				out.writeCharacters(fl.getOrigin());
				out.writeEndElement();

 				out.writeStartElement("visualParameters");
					out.writeStartElement("stemColour");
					out.writeCharacters(fl.getVisualParameters().getStemColour());
					out.writeEndElement();

					out.writeStartElement("leafColour");
					out.writeCharacters(fl.getVisualParameters().getLeafColour());
					out.writeEndElement();

					out.writeStartElement("aveLenFlower");
					out.writeAttribute("measure"
							, fl.getVisualParameters().getMeasure());
					out.writeCharacters(String.valueOf(fl.getVisualParameters().getAveLenFlower()));
					out.writeEndElement();

				out.writeEndElement();

				out.writeStartElement("growingTips");
					out.writeStartElement("tempreture");
					out.writeAttribute("measure"
					, fl.getGrowingTips().getTemperatureMeasure());
					out.writeCharacters(String.valueOf(fl.getGrowingTips().getTempreture()));
					out.writeEndElement();

					out.writeStartElement("lighting");
					out.writeAttribute("lightRequiring"
					, fl.getGrowingTips().getLighting().getTitle());
//					out.writeCharacters(String.valueOf(fl.getGrowingTips().getTempreture()));
					out.writeEndElement();

					out.writeStartElement("watering");
					out.writeAttribute("measure"
					, fl.getGrowingTips().getWateringMeasure());
					out.writeCharacters(String.valueOf(fl.getGrowingTips().getWatering()));
					out.writeEndElement();

				out.writeEndElement();

				out.writeStartElement("multiplying");
				out.writeCharacters(fl.getMultiplying().getTitle());
				out.writeEndElement();

			out.writeEndElement();
		}

		out.writeEndElement();
		out.writeEndDocument();

		out.close();
*/	}

	public Flowers loadData() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		SAXController saxHandler = this;//new SAXController("");
		saxParser.parse(new File(xmlFileName), saxHandler);

		return flowers;
	}


	@Override
	public void startDocument() throws SAXException {
		flowers = new Flowers();
	}

	@Override
	public void startElement(String uri, String lName, String qName
			, Attributes attr) throws SAXException {
		switch (qName) {
			case "flowers":
				flowers.setAttributes(new LinkedHashMap<>());
				flowers.setFlowers(new ArrayList<>());
				for(int i=0; i<attr.getLength(); i++){
					flowers.getAttributes().put(attr.getLocalName(i)
					, attr.getValue(i));
				}
				break;
			case "flower":
				flower = new Flower();
			break;
			case "name":
				elementValue = new StringBuilder();
				break;
			case "soil":
				elementValue = new StringBuilder();
				break;
			case "origin":
				elementValue = new StringBuilder();
				break;
			case "visualParameters":
				visualParameters = new VisualParameters();
				break;
			case "stemColour":
				elementValue = new StringBuilder();
				break;
			case "leafColour":
				elementValue = new StringBuilder();
				break;
			case "aveLenFlower":
				elementValue = new StringBuilder();
				visualParameters.setMeasure(attr.getValue("measure"));
				break;
			case "growingTips":
				growingTips = new GrowingTips();
				break;
			case "tempreture":
				elementValue = new StringBuilder();
				growingTips.setTemperatureMeasure(attr.getValue("measure"));
				break;
			case "lighting":
				elementValue = new StringBuilder();
				growingTips.setLighting(Lighting.getByValue(
						attr.getValue("lightRequiring")));
				break;
			case "watering":
				elementValue = new StringBuilder();
				growingTips.setWateringMeasure(attr.getValue("measure"));
				break;
			case "multiplying":
				elementValue = new StringBuilder();
				break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName) {
			case "flower":
				flowers.getFlowers().add(flower);
				break;
			case "name":
				flower.setName(elementValue.toString());
				break;
			case "soil":
				flower.setSoil(Soil.getByValue(elementValue.toString()));
				break;
			case "origin":
				flower.setOrigin(elementValue.toString());
				break;
			case "visualParameters":
				flower.setVisualParameters(visualParameters);
				break;
			case "stemColour":
				visualParameters.setStemColour(elementValue.toString());
				break;
			case "leafColour":
				visualParameters.setLeafColour(elementValue.toString());
				break;
			case "aveLenFlower":
				visualParameters.setAveLenFlower(Integer.parseInt(elementValue.toString()));
				break;
			case "growingTips":
				flower.setGrowingTips(growingTips);
				break;
			case "tempreture":
				growingTips.setTempreture(Integer.parseInt(elementValue.toString()));
				break;
			case "lighting":
				break;
			case "watering":
				growingTips.setWatering(Integer.parseInt(elementValue.toString()));
				break;
			case "multiplying":
				flower.setMultiplying(Multiplying.getByValue(elementValue.toString()));
//				elementValue = new StringBuilder();
				break;
		}
	}

}