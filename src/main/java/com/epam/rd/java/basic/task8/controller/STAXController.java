package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public void setXmlFileName(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void saveToXml(Flowers flowers) throws FileNotFoundException, XMLStreamException, UnsupportedEncodingException {
        OutputStream outputStream = new FileOutputStream(new File(xmlFileName));

        XMLStreamWriter out = XMLOutputFactory.newInstance().createXMLStreamWriter(
                new OutputStreamWriter(outputStream, "utf-8"));

        out.writeStartDocument();
        out.writeStartElement("flowers");
        for(Map.Entry<String, String> att:flowers.getAttributes().entrySet()) {
            out.writeAttribute(att.getKey(), att.getValue());
        }

        for (Flower fl: flowers.getFlowers()) {
            out.writeStartElement("flower");
            out.writeStartElement("name");
            out.writeCharacters(fl.getName());
            out.writeEndElement();

            out.writeStartElement("soil");
            out.writeCharacters(fl.getSoil().getTitle());
            out.writeEndElement();

            out.writeStartElement("origin");
            out.writeCharacters(fl.getOrigin());
            out.writeEndElement();

            out.writeStartElement("visualParameters");
            out.writeStartElement("stemColour");
            out.writeCharacters(fl.getVisualParameters().getStemColour());
            out.writeEndElement();

            out.writeStartElement("leafColour");
            out.writeCharacters(fl.getVisualParameters().getLeafColour());
            out.writeEndElement();

            out.writeStartElement("aveLenFlower");
            out.writeAttribute("measure"
                    , fl.getVisualParameters().getMeasure());
            out.writeCharacters(String.valueOf(fl.getVisualParameters().getAveLenFlower()));
            out.writeEndElement();

            out.writeEndElement();

            out.writeStartElement("growingTips");
            out.writeStartElement("tempreture");
            out.writeAttribute("measure"
                    , fl.getGrowingTips().getTemperatureMeasure());
            out.writeCharacters(String.valueOf(fl.getGrowingTips().getTempreture()));
            out.writeEndElement();

            out.writeStartElement("lighting");
            out.writeAttribute("lightRequiring"
                    , fl.getGrowingTips().getLighting().getTitle());
//					out.writeCharacters(String.valueOf(fl.getGrowingTips().getTempreture()));
            out.writeEndElement();

            out.writeStartElement("watering");
            out.writeAttribute("measure"
                    , fl.getGrowingTips().getWateringMeasure());
            out.writeCharacters(String.valueOf(fl.getGrowingTips().getWatering()));
            out.writeEndElement();

            out.writeEndElement();

            out.writeStartElement("multiplying");
            out.writeCharacters(fl.getMultiplying().getTitle());
            out.writeEndElement();

            out.writeEndElement();
        }

        out.writeEndElement();
        out.writeEndDocument();

        out.close();
    }

    public Flowers loadData() throws FileNotFoundException, XMLStreamException {
        Flowers flowers = new Flowers();
        Flower flower =new Flower();
        VisualParameters visualParameters = new VisualParameters();
        GrowingTips growingTips = new GrowingTips();
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        XMLEvent nextEvent;
        StartElement startElement;
        EndElement endElement;

        while (reader.hasNext()) {
            nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                startElement = nextEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "flowers":
                        flowers.setAttributes(new LinkedHashMap<>());
                        flowers.setFlowers(new ArrayList<>());

                        Iterator<Namespace> namespaceIterator = startElement.getNamespaces();
                        while (namespaceIterator.hasNext()) {
                            Namespace namespace= namespaceIterator.next();
                            String key = namespace.getName().getPrefix();
                            key +=  !namespace.getName().getLocalPart().isBlank()
                                    ? ":" + namespace.getName().getLocalPart()
                                    : "";
                            flowers.getAttributes().put(
                                    key
                                    , namespace.getValue());
                        }

                        Iterator<Attribute> attributeIterator = startElement.getAttributes();
                        Attribute attribute;
                        while (attributeIterator.hasNext()) {
                            attribute = attributeIterator.next();
                            flowers.getAttributes().put(
                                    attribute.getName().getPrefix() +
                                            (!attribute.getName().getLocalPart().isBlank()
                                                    ? ":" + attribute.getName().getLocalPart()
                                                    : "")
                                    , attribute.getValue());
                        }
                        break;
                    case "flower":
                        flower = new Flower();
                        break;
                    case "name":
                        nextEvent = reader.nextEvent();
                        flower.setName(nextEvent.asCharacters().getData());
                        break;
                    case "soil":
                        nextEvent = reader.nextEvent();
                        flower.setSoil(Soil.getByValue(nextEvent.asCharacters().getData()));
                        break;
                    case "origin":
                        nextEvent = reader.nextEvent();
                        flower.setOrigin(nextEvent.asCharacters().getData());
                        break;
                    case "visualParameters":
                        visualParameters = new VisualParameters();
                        break;
                    case "stemColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setStemColour(nextEvent.asCharacters().getData());
                        break;
                    case "leafColour":
                        nextEvent = reader.nextEvent();
                        visualParameters.setLeafColour(nextEvent.asCharacters().getData());
                        break;
                    case "aveLenFlower":
                        attribute = startElement.getAttributeByName(
                                new QName("measure"));
                        visualParameters.setMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        visualParameters.setAveLenFlower(
                                Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "growingTips":
                        growingTips = new GrowingTips();
                        break;
                    case "tempreture":
                        attribute = startElement.getAttributeByName(
                                new QName("measure"));
                        growingTips.setTemperatureMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        growingTips.setTempreture(
                                Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "lighting":
//                        nextEvent = reader.nextEvent();
                        attribute = startElement.getAttributeByName(
                                new QName("lightRequiring"));
                        growingTips.setLighting(Lighting.getByValue(attribute.getValue()));
                        break;
                    case "watering":
                        attribute = startElement.getAttributeByName(
                                new QName("measure"));
                        growingTips.setWateringMeasure(attribute.getValue());
                        nextEvent = reader.nextEvent();
                        growingTips.setWatering(
                                Integer.parseInt(nextEvent.asCharacters().getData()));
                        break;
                    case "multiplying":
                        nextEvent = reader.nextEvent();
                        flower.setMultiplying(Multiplying.getByValue(
                                nextEvent.asCharacters().getData()));
                        break;
                    //...
                }
            } else if (nextEvent.isEndElement()) {
                endElement = nextEvent.asEndElement();
                switch (endElement.getName().getLocalPart()) {
                    case "growingTips":
                        flower.setGrowingTips(growingTips);
                        break;
                    case "visualParameters":
                        flower.setVisualParameters(visualParameters);
                        break;
                    case "flower":
                        flowers.getFlowers().add(flower);
                        break;
                    case "flowers":
                        return flowers;
//                    break;
                }
            }
        }
        return flowers;
    }
}