package com.epam.rd.java.basic.task8.controller;


//import javax.swing.text.Document;
import com.epam.rd.java.basic.task8.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.*;


/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public void setXmlFileName(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void saveToXml(Flowers flowers){
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
//		dbf.setNamespaceAware(true);
		DocumentBuilder db = null;
		Document document = null;
		try  {
			db = dbf.newDocumentBuilder();
			document = db.newDocument();

			Element root = document.createElement("flowers");
			document.appendChild(root);
			for(Map.Entry<String,String> flowersAttribute:flowers.getAttributes().entrySet()) {
				root.setAttribute(flowersAttribute.getKey(),
						flowersAttribute.getValue());
			}

			for (Flower fl:flowers.getFlowers()) {

				Element flower = document.createElement("flower");
				root.appendChild(flower);

				Element name = document.createElement("name");
				name.appendChild(document.createTextNode(fl.getName()));
				flower.appendChild(name);

				Element soil = document.createElement("soil");
				soil.appendChild(document.createTextNode(fl.getSoil().getTitle()));
				flower.appendChild(soil);

				Element origin = document.createElement("origin");
				origin.appendChild(document.createTextNode(fl.getOrigin()));
				flower.appendChild(origin);

				Element visualParameters = document.createElement("visualParameters");
//				origin.appendChild(document.createTextNode(fl.getOrigin()));
				flower.appendChild(visualParameters);

				Element stemColour = document.createElement("stemColour");
				stemColour.appendChild(document.createTextNode(fl.getVisualParameters().getStemColour()));
				visualParameters.appendChild(stemColour);

				Element leafColour = document.createElement("leafColour");
				leafColour.appendChild(document.createTextNode(fl.getVisualParameters().getLeafColour()));
				visualParameters.appendChild(leafColour);

				Element aveLenFlower = document.createElement("aveLenFlower");
				aveLenFlower.appendChild(document
						.createTextNode(String.valueOf(fl.getVisualParameters()
								.getAveLenFlower())));
				visualParameters.appendChild(aveLenFlower);
				aveLenFlower.setAttribute("measure",
						fl.getVisualParameters().getMeasure());

				Element growingTips = document.createElement("growingTips");
//				origin.appendChild(document.createTextNode(fl.getOrigin()));
				flower.appendChild(growingTips);

				Element tempreture = document.createElement("tempreture");
				tempreture.appendChild(document.createTextNode(String
						.valueOf(fl.getGrowingTips().getTempreture())));
				growingTips.appendChild(tempreture);
				tempreture.setAttribute("measure",
						fl.getGrowingTips().getTemperatureMeasure());

				Element lighting = document.createElement("lighting");
/*				lighting.appendChild(document.createTextNode(
						fl.getGrowingTips().getLighting().getTitle()));
*/				growingTips.appendChild(lighting);
				lighting.setAttribute("lightRequiring",
						fl.getGrowingTips().getLighting().getTitle());


				Element watering = document.createElement("watering");
				watering.appendChild(document.createTextNode(
						String.valueOf(fl.getGrowingTips().getWatering())));
				growingTips.appendChild(watering);
				watering.setAttribute("measure",
						fl.getGrowingTips().getWateringMeasure());

				Element multiplying = document.createElement("multiplying");
				multiplying.appendChild(document.createTextNode(
						fl.getMultiplying().toString()));
				flower.appendChild(multiplying);

			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource domSource = new DOMSource(document);
			StreamResult streamResult = new StreamResult(new File(xmlFileName));
			transformer.transform(domSource, streamResult);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (SAXException e) {
//			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}

	public Comparator<Flower> compareFlowerByName = (f1, f2) -> f1.getName()
			.compareTo(f2.getName());

	public Comparator<Flower> compareFlowerBySoil = (f1, f2) -> f1.getSoil()
			.compareTo(f2.getSoil());

	public Comparator<Flower> compareFlowerByOrigin = (f1, f2) -> f1.getOrigin()
			.compareTo(f2.getOrigin());

	public Flowers loadData () {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = null;
		Document document = null;
		try  {
			db = dbf.newDocumentBuilder();
			document = db.parse(xmlFileName);
//			document = db.parse(new File(xmlFileName));
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		Node n = null;

		Flowers flowersRoot = new Flowers();
		NodeList nl = document.getElementsByTagName("flowers");
 		n = nl.item(0);
 		NamedNodeMap attributes = n.getAttributes();
 		for (int i =0; i<attributes.getLength(); i++){
 			flowersRoot.getAttributes().put(
					attributes.item(i).getNodeName(),
					attributes.item(i).getNodeValue()
			);
		}

		nl = document.getElementsByTagName("flower");
		List<Flower> flowers = new ArrayList<>();
		Flower tempFlower = new Flower();
		NodeList flowerNodes = null;
		String name,soil,origin;

		for (int i=0; i< nl.getLength(); i++) {
			n = nl.item(i);
			tempFlower = new Flower();
			if(parseFlower(n, tempFlower)){
				flowers.add(tempFlower);
			}
		}

		flowersRoot.setFlowers(flowers);

	return flowersRoot;
	}

	private boolean parseFlower(Node nn, Flower flower){
		int nodesCounter = 0;
		int parameterCounter = 0;
		NodeList nl = nn.getChildNodes();
		Node n;
		String textInfo, textData;
		for(nodesCounter = 0; nodesCounter < nl.getLength(); nodesCounter++){
			n = nl.item(nodesCounter);
			if(n.getNodeType()==Node.ELEMENT_NODE){//Node.TEXT_NODE){
				textInfo = n.getNodeName();
				if(textInfo.equals("name")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					flower.setName(textData);
					parameterCounter++;
				} else if(textInfo.equals("soil")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					Soil soil = Soil.getByValue(textData);
					flower.setSoil(soil);
					parameterCounter++;
				} else if(textInfo.equals("origin")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					flower.setOrigin(textData);
					parameterCounter++;
				} else if(textInfo.equals("visualParameters")){
//					Node firstChildNode = n.getFirstChild();
					parseVisualParameters(n, flower);
					parameterCounter++;
				} else if(textInfo.equals("growingTips")){
//					Node firstChildNode = n.getFirstChild();
					parseGrowingTips(n, flower);
					parameterCounter++;
				} else if(textInfo.equals("multiplying")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					Multiplying multiplying = Multiplying.getByValue(textData);
					flower.setMultiplying(multiplying);
					parameterCounter++;
				}
			}
		}
		if (parameterCounter>4)
			return true;
		else
			return false;
	}


	private boolean parseGrowingTips (Node nn, Flower flower){
		int nodesCounter = 0;
		int parameterCounter = 0;
		NodeList nl = nn.getChildNodes();
		Node n;
		String textInfo, textData;
		GrowingTips growingTips = new GrowingTips();
		NamedNodeMap attributes;
		for(nodesCounter = 0; nodesCounter < nl.getLength(); nodesCounter++){
			n = nl.item(nodesCounter);
			if(n.getNodeType()==Node.ELEMENT_NODE){//Node.TEXT_NODE){
				textInfo = n.getNodeName();
				if(textInfo.equals("tempreture")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					growingTips.setTempreture(Integer.parseInt(textData));
					attributes = n.getAttributes();
					growingTips.setTemperatureMeasure(attributes
							.getNamedItem("measure").getNodeValue());
					parameterCounter++;
				} else if(textInfo.equals("lighting")){
//					Node firstChildNode = n.getFirstChild();
//					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					textData = n.getAttributes().getNamedItem("lightRequiring").getNodeValue();
					growingTips.setLighting(Lighting.getByValue(textData));
					parameterCounter++;
				} else if(textInfo.equals("watering")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					growingTips.setWatering(Integer.parseInt(textData));
					attributes = n.getAttributes();
					growingTips.setWateringMeasure(attributes
							.getNamedItem("measure").getNodeValue());
					parameterCounter++;
				}
			}
		}
		if (parameterCounter>2){
			flower.setGrowingTips(growingTips);
			return true;
		} else
			return false;
	}



	private boolean parseVisualParameters (Node nn, Flower flower){
		int nodesCounter = 0;
		int parameterCounter = 0;
		NodeList nl = nn.getChildNodes();
		Node n;
		String textInfo, textData;
		VisualParameters visualParameters = new VisualParameters();
		for(nodesCounter = 0; nodesCounter < nl.getLength(); nodesCounter++){
			n = nl.item(nodesCounter);
			if(n.getNodeType()==Node.ELEMENT_NODE){//Node.TEXT_NODE){
				textInfo = n.getNodeName();
				if(textInfo.equals("stemColour")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					visualParameters.setStemColour(textData);
					parameterCounter++;
				} else if(textInfo.equals("leafColour")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					visualParameters.setLeafColour(textData);
					parameterCounter++;
				} else if(textInfo.equals("aveLenFlower")){
					Node firstChildNode = n.getFirstChild();
					textData = firstChildNode.getNodeValue().replace("\n","").trim();
					visualParameters.setAveLenFlower(Integer.parseInt(textData));
					NamedNodeMap attributes = n.getAttributes();
					visualParameters.setMeasure(attributes
							.getNamedItem("measure").getNodeValue());
					parameterCounter++;
				}
			}
		}
		if (parameterCounter>1){
			flower.setVisualParameters(visualParameters);
			return true;
		} else
			return false;
	}


}
